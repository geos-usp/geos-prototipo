---
title: Apresentação BitTorrent
author: gui
attendees:
    - gui
    - misterio
    - tomieiro
    - deandreson
    - julio
---

- **Apresentação** 

    - [BitTorrent.pdf](/assets/bittorrent.pdf) 
    - [BitTorrrent.odp](/assets/bittorrent.odp)

- **Participação:**

    - Slides e Apresentação: [Guilherme Paixão](https://guip.dev/)
    - Workshop *seedbox*: [Gabriel Fontes](https://misterio.me/)
